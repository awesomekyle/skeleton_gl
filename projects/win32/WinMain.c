/*! @file WinMain.c
 *  @copyright Copyright (c) 2014 Kyle. All rights reserved.
 */
#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#include <windowsx.h>
#include <Shellapi.h>
#include "gl_include.h"
#include "system.h"
#include "game.h"

/* Internal 
 */
static const char _class_name[] = "engine";

/* Variables 
 */
static HCURSOR  _cursor = NULL;
static HWND     _hwnd = NULL;
static HDC      _dc = NULL;
static uint32_t _width = 0;
static uint32_t _height = 0;
static int      _fullscreen = 0;
static int      _prev_mouse_x = -1;
static int      _prev_mouse_y = -1;
static Game*    _game = NULL;

/* Functions
 */
static LRESULT CALLBACK _WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );

static Key convert_keycode(uint8_t key)
{   
    if(key >= 'A' && key <= 'Z')
        return (Key)((key - 'A') + KEY_A);
    
    if(key >= '0' && key <= '9')
        return (Key)((key - '0') + KEY_0);

    if(key >= VK_F1 && key <= VK_F12)
        return (Key)((key - VK_F1) + KEY_F1);

    switch(key)
    {
    case VK_ESCAPE:
        return KEY_ESCAPE;

    case VK_SPACE:  
        return KEY_SPACE;

    case VK_SHIFT:
    case VK_LSHIFT:
    case VK_RSHIFT:
        return KEY_SHIFT;
    case VK_CONTROL:
    case VK_LCONTROL:
    case VK_RCONTROL:
        return KEY_CTRL;
    case VK_MENU:
    case VK_LMENU:
    case VK_RMENU:
        return KEY_ALT;

    case VK_RETURN:
        return KEY_ENTER;

    case VK_LEFT:
        return KEY_LEFT;
    case VK_RIGHT:
        return KEY_RIGHT;
    case VK_UP:
        return KEY_UP;
    case VK_DOWN:
        return KEY_DOWN;

    default:
        return KEY_INVALID;
    }
}
static void _initialize_opengl(HWND hwnd)
{
    HWND hWnd = (HWND)hwnd;
    HDC hDC = GetDC(hWnd);

    PIXELFORMATDESCRIPTOR   pfd = {0};
    pfd.nSize       = sizeof(pfd);
    pfd.nVersion    = 1;
    pfd.dwFlags     = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
    pfd.iPixelType  = PFD_TYPE_RGBA;
    pfd.cColorBits  = 24;
    pfd.cDepthBits  = 24;
    pfd.iLayerType  = PFD_MAIN_PLANE;

    int pixel_format = ChoosePixelFormat(hDC, &pfd);
    assert(pixel_format);

    int result = SetPixelFormat(hDC, pixel_format, &pfd);
    assert(result);
    if(!result) {
        sys_log("SetPixelFormat failed\n");
    }

    // Create a dummy OpenGL 1.1 context to use for Glew initialization
    HGLRC first_GLRC = wglCreateContext(hDC);
    assert(first_GLRC);
    wglMakeCurrent(hDC, first_GLRC);
    CheckGLError();

    // Glew
    GLenum error = glewInit();
    CheckGLError();
    if(error != GLEW_OK) {
        sys_log("Glew Error: %s\n", glewGetErrorString(error));
    }
    assert(error == GLEW_OK);
    assert(wglewIsSupported("WGL_ARB_extensions_string") == 1);
    assert(wglewIsSupported("WGL_ARB_create_context") == 1);

    // Now create the real, OpenGL 3.2 context
    GLint attributes[] = {
        WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
        WGL_CONTEXT_MINOR_VERSION_ARB, 2,
        0,
    };
    HGLRC new_GLRC = wglCreateContextAttribsARB(hDC, 0, attributes);
    assert(new_GLRC);
    CheckGLError();

    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(first_GLRC);
    wglMakeCurrent(hDC, new_GLRC);
    CheckGLError();
    _dc = hDC;

    // Disable vsync
    assert(wglewIsSupported("WGL_EXT_swap_control") == 1);
    wglSwapIntervalEXT(0);
    CheckGLError();
}
static void _create_application(HINSTANCE hInstance, const char* name)
{
    WNDCLASSEX wcex;
    wcex.cbSize         = sizeof(WNDCLASSEX); 
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = _WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = NULL;
    wcex.hCursor        = (HCURSOR)LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = NULL;
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = name;
    wcex.hIconSm        = NULL;
    if(!RegisterClassEx(&wcex))
        MessageBox(NULL, "Could not register class", "Error", MB_OK);
}
static void _find_screen_top_left(int* x, int* y) 
{
    RECT rect;
    HWND taskBar;
    int width;

    *y = 0;
    *x = 0;
    taskBar = FindWindow("Shell_traywnd", NULL);
    width = GetSystemMetrics(SM_CXSCREEN);

    if(taskBar && GetWindowRect(taskBar, &rect)) {
        if(rect.right == width && rect.top == 0) /* Top taskbar */
            *y = rect.bottom;
        else if(rect.right != width) /* Left taskbar */
            *x = rect.right;
    }
}

static HWND _create_window(HINSTANCE hInstance, const char* name) 
{
    RECT rect;
    HWND hwnd;
    int width = GetSystemMetrics(SM_CXSCREEN);
    int height = GetSystemMetrics(SM_CYSCREEN);
    UINT uiStyle = WS_OVERLAPPEDWINDOW;
    int x;
    int y;
    _find_screen_top_left(&x, &y);

    rect.left = 0;
    rect.top = 0;
    rect.right = width/2;
    rect.bottom = height/2;

    AdjustWindowRect(&rect, uiStyle, FALSE);
    hwnd = CreateWindow(name, 
                        "Main Window", 
                        uiStyle, 
                        x, y,
                        rect.right-rect.left, 
                        rect.bottom-rect.top, 
                        NULL, NULL, 
                        hInstance, NULL);
    if(hwnd == NULL)
        MessageBox(NULL, "Could not create window", "Error", MB_OK);
    return hwnd;
}

static void _toggle_fullscreen(void) 
{
    _fullscreen = !_fullscreen;
    if(_fullscreen) {
        int width = GetSystemMetrics(SM_CXSCREEN);
        int height = GetSystemMetrics(SM_CYSCREEN);
        SetWindowLongPtr(_hwnd, GWL_STYLE, 
            WS_SYSMENU | WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE); 
        MoveWindow(_hwnd, 0, 0, width, height, TRUE); 
    } else {
        int width = GetSystemMetrics(SM_CXSCREEN);
        int height = GetSystemMetrics(SM_CYSCREEN);
        RECT rect; 
        rect.left = 0; 
        rect.top = 0; 
        rect.right = width/2; 
        rect.bottom = height/2; 
        SetWindowLongPtr(_hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE); 
        AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE); 
        MoveWindow(_hwnd, 0, 0, rect.right-rect.left, rect.bottom-rect.top, TRUE);
    }
}

LRESULT CALLBACK _WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam ) 
{
    Key key;
    switch(message) {
    case WM_CREATE: 
        return 0;
    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
        key = convert_keycode( (uint8_t)wParam );
        if( wParam == VK_RETURN ) {
            if( ( HIWORD( lParam ) & KF_ALTDOWN ) ) {
                _toggle_fullscreen();
                return 0;
            }
        }
        if( key == KEY_INVALID )
            break;
        if( sys_key_status( key ) == 0 ) {
            SystemEvent event;
            event.type = EVENT_KEYDOWN;
            event.data.key = key;
            _sys_push_event( event );
        }
        _sys_set_key_status(key, 1);
        break;
    case WM_KEYUP:
    case WM_SYSKEYUP:
        key = convert_keycode((uint8_t)wParam);
        if(key == KEY_INVALID)
            return 0;
        _sys_set_key_status(key, 0);
        break;
    case WM_MENUCHAR: /* identify alt+enter, make it not beep since we're handling it: */
        if( LOWORD(wParam) & VK_RETURN )
            return MAKELRESULT(0, MNC_CLOSE);
        return MAKELRESULT(0, MNC_IGNORE);
    case WM_CLOSE:
        PostQuitMessage(0);
        return 0;
    case WM_ERASEBKGND:
    case WM_SIZING:
        return 0;
    case WM_SIZE:
        {
            SystemEvent event;
            RECT rect;
            GetClientRect(_hwnd, &rect);
            _width = rect.right - rect.left;
            _height = rect.bottom - rect.top;
            event.data.resize.width = _width;
            event.data.resize.height = _height;
            event.type = EVENT_RESIZE;
            _sys_push_event(event);
        }
        return 0;
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_RBUTTONDOWN:
    case WM_RBUTTONUP:
    case WM_MBUTTONDOWN:
    case WM_MBUTTONUP:
        {
            SystemEvent event;
            int x = GET_X_LPARAM(lParam);
            int y = _height - GET_Y_LPARAM(lParam);
            event.data.mouse.x = (float)x;
            event.data.mouse.y = (float)y;
            // L button
            if(wParam & MK_LBUTTON) {
                if(sys_mouse_button_status(MOUSE_LEFT) == 0) {
                    event.type = EVENT_MOUSE_DOWN;
                    event.data.mouse.button = MOUSE_LEFT;
                    _sys_push_event(event);
                }
                _sys_set_mouse_button_status(MOUSE_LEFT, 1);
            } else {
                if(sys_mouse_button_status(MOUSE_LEFT) == 1) {
                    event.type = EVENT_MOUSE_UP;
                    event.data.mouse.button = MOUSE_LEFT;
                    _sys_push_event(event);
                }
                _sys_set_mouse_button_status(MOUSE_LEFT, 0);
            }
            // R button
            if(wParam & MK_RBUTTON) {
                if(sys_mouse_button_status(MOUSE_RIGHT) == 0) {
                    event.type = EVENT_MOUSE_DOWN;
                    event.data.mouse.button = MOUSE_RIGHT;
                    _sys_push_event(event);
                }
                _sys_set_mouse_button_status(MOUSE_RIGHT, 1);
            } else {
                if(sys_mouse_button_status(MOUSE_RIGHT) == 1) {
                    event.type = EVENT_MOUSE_UP;
                    event.data.mouse.button = MOUSE_RIGHT;
                    _sys_push_event(event);
                }
                _sys_set_mouse_button_status(MOUSE_RIGHT, 0);
            }
            // M button
            if(wParam & MK_MBUTTON) {
                if(sys_mouse_button_status(MOUSE_MIDDLE) == 0) {
                    event.type = EVENT_MOUSE_DOWN;
                    event.data.mouse.button = MOUSE_MIDDLE;
                    _sys_push_event(event);
                }
                _sys_set_mouse_button_status(MOUSE_MIDDLE, 1);
            } else {
                if(sys_mouse_button_status(MOUSE_MIDDLE) == 1) {
                    event.type = EVENT_MOUSE_UP;
                    event.data.mouse.button = MOUSE_MIDDLE;
                    _sys_push_event(event);
                }
                _sys_set_mouse_button_status(MOUSE_MIDDLE, 0);
            }
        }
        return 0;
    case WM_MOUSEMOVE:
        {
            SystemEvent event;
            int x = GET_X_LPARAM(lParam);
            int y = _height - GET_Y_LPARAM(lParam);
            event.data.mouse.x = (float)(x - _prev_mouse_x);
            event.data.mouse.y = (float)(_prev_mouse_y - y);
            event.type = EVENT_MOUSE_MOVE;
            _sys_push_event(event);

            _prev_mouse_x = x;
            _prev_mouse_y = y;
        }
        return 0;
    }
    return DefWindowProc(hWnd, message, wParam, lParam);
}

/*  External 
 */
void sys_lock_and_hide_cursor(void)
{
    RECT rect;
    GetClientRect(_hwnd, &rect);
    ClipCursor(&rect);
    SetCursor(NULL);
}
void sys_unlock_and_show_cursor(void)
{
    SetCursor(_cursor);
    ClipCursor(NULL);
}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) 
{
    MSG msg = {0};
    _create_application(hInstance, _class_name);
    _hwnd = _create_window(hInstance, _class_name);
    ShowWindow(_hwnd, SW_SHOWNORMAL);

    /* Initialize OpenGL */
    _initialize_opengl(_hwnd);

    /* TODO: Initialization code */
    _game = create_game();

    /* Main loop */
    do {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            TranslateMessage(&msg); 
            DispatchMessage(&msg); 
        } else {
            /* TODO: Per-frame code */
            if(update_game(_game))
                break;
            render_game(_game);
            SwapBuffers(_dc);
            CheckGLError();
        }
    } while(msg.message != WM_QUIT);

    /* TODO: Shutdown code */
    destroy_game(_game);

    (void)(hPrevInstance);
    (void)(lpCmdLine);
    (void)(nCmdShow);
    return 0;
}
