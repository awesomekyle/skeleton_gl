/** @file OpenGLView.h
 *  @brief OpenGL view
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#import <Cocoa/Cocoa.h>

@interface OSXOpenGLView : NSOpenGLView {
    NSTrackingRectTag trackingRect;
}

@property GLint currentVirtualScreen;

@end
