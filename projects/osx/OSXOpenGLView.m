/** @file OpenGLView.m
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#import "OSXOpenGLView.h"
#import "AppDelegate.h"
#import <OpenGL/gl3.h>

@implementation OSXOpenGLView

- (void) awakeFromNib
{
    NSOpenGLPixelFormatAttribute attributes[] =
    {
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,/* OpenGL 3.2 profile */
        NSOpenGLPFAAccelerated,                                 /* Only use hardware acceleration */
        NSOpenGLPFADoubleBuffer,                                /* Double buffer it */
        NSOpenGLPFAColorSize, (NSOpenGLPixelFormatAttribute)32, /* 32-bit color buffer */
        NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)24, /* 24-bit depth buffer */
        NSOpenGLPFAAllowOfflineRenderers,                       /* Support offline renderers (on integrated+discrete Macs) */
        (NSOpenGLPixelFormatAttribute)nil,
    };
    NSOpenGLPixelFormat*    pixelFormat = nil;
    NSOpenGLContext*        context = nil;

    pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];
    if(pixelFormat == nil)
        NSLog(@"Creating pixel format failed!");

    context = [[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil];

    CGLEnable((CGLContextObj)[context CGLContextObj], kCGLCECrashOnRemovedFunctions);
    [self setPixelFormat:pixelFormat];
    [self setOpenGLContext:context];
    [self setWantsBestResolutionOpenGLSurface:YES];
}
- (void)setFrame:(NSRect)frame {
    [super setFrame:frame];
    [self removeTrackingRect:trackingRect];
    trackingRect = [self addTrackingRect:[self bounds] owner:self userData:NULL assumeInside:NO];
}
 
- (void)setBounds:(NSRect)bounds {
    [super setBounds:bounds];
    [self removeTrackingRect:trackingRect];
    trackingRect = [self addTrackingRect:[self bounds] owner:self userData:NULL assumeInside:NO];
}
- (BOOL) acceptsFirstResponder
{
    return YES;
}
- (void) prepareOpenGL
{
    [super prepareOpenGL];
    [((AppDelegate*)[[NSApplication sharedApplication] delegate]) initDisplayLinkWithView:self];
}

- (void) update
{
    GLint newVirtualScreen = 0;
    [super update];

    newVirtualScreen = [[self openGLContext] currentVirtualScreen];
    if (_currentVirtualScreen != newVirtualScreen)
    {
        _currentVirtualScreen = newVirtualScreen;
        /* Adapt to any changes in capabilities
         * (such as max texture size and hardware capabilities). */
        NSLog(@"New rendrerer: %s", glGetString(GL_RENDERER));
    }
}
- (void)mouseEntered:(NSEvent *)theEvent
{
    [[self window] setAcceptsMouseMovedEvents:YES];
}
- (void)mouseExited:(NSEvent *)theEvent
{
    [[self window] setAcceptsMouseMovedEvents:NO];
}
-(void)mouseMoved:(NSEvent *)theEvent
{
    [[self window] mouseMoved:theEvent];
}

@end
