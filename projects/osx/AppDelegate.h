#import <Cocoa/Cocoa.h>
#include "game.h"

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    CVDisplayLinkRef displayLink;
    Game*    _game;
}

@property (assign) IBOutlet NSWindow *window;
@property  NSOpenGLView* view;

-(void) initDisplayLinkWithView:(NSOpenGLView*)view;
-(void) shutdownDisplayLink;
-(void) update;
-(void) render;
-(void) windowResized:(NSNotification *)notification;

@end
