/** @file WindowController.h
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#import <Cocoa/Cocoa.h>

@interface WindowController : NSWindowController

@end
