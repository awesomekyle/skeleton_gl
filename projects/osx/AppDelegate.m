#import "AppDelegate.h"
#include "system.h"

#define KEY_MAPPING(keyCode, sysKey) case keyCode: return sysKey;
static Key _convert_keycode(uint16_t key) {
    /* Reference: http://boredzo.org/blog/wp-content/uploads/2007/05/IMTx-virtual-keycodes.pdf */
    switch(key) {
    /* Qwerty row */
    KEY_MAPPING(12, KEY_Q);
    KEY_MAPPING(13, KEY_W);
    KEY_MAPPING(14, KEY_E);
    KEY_MAPPING(15, KEY_R);
    KEY_MAPPING(17, KEY_T);
    KEY_MAPPING(16, KEY_Y);
    KEY_MAPPING(32, KEY_U);
    KEY_MAPPING(34, KEY_I);
    KEY_MAPPING(31, KEY_O);
    KEY_MAPPING(35, KEY_P);

    /* Asdf row */
    KEY_MAPPING(0, KEY_A);
    KEY_MAPPING(1, KEY_S);
    KEY_MAPPING(2, KEY_D);
    KEY_MAPPING(3, KEY_F);
    KEY_MAPPING(5, KEY_G);
    KEY_MAPPING(4, KEY_H);
    KEY_MAPPING(38, KEY_J);
    KEY_MAPPING(40, KEY_K);
    KEY_MAPPING(37, KEY_L);

    /* Zxcv row */
    KEY_MAPPING(6, KEY_Z);
    KEY_MAPPING(7, KEY_X);
    KEY_MAPPING(8, KEY_C);
    KEY_MAPPING(9, KEY_V);
    KEY_MAPPING(11, KEY_B);
    KEY_MAPPING(45, KEY_N);
    KEY_MAPPING(46, KEY_M);

    /* Numbers */
    KEY_MAPPING(18, KEY_1);
    KEY_MAPPING(19, KEY_2);
    KEY_MAPPING(20, KEY_3);
    KEY_MAPPING(21, KEY_4);
    KEY_MAPPING(23, KEY_5);
    KEY_MAPPING(22, KEY_6);
    KEY_MAPPING(26, KEY_7);
    KEY_MAPPING(28, KEY_8);
    KEY_MAPPING(25, KEY_9);
    KEY_MAPPING(29, KEY_0);

    /* Misc */
    KEY_MAPPING(53, KEY_ESCAPE);
    KEY_MAPPING(56, KEY_SHIFT);
    KEY_MAPPING(59, KEY_CTRL);
    KEY_MAPPING(58, KEY_ALT);
    KEY_MAPPING(49, KEY_SPACE);

    KEY_MAPPING(55, KEY_SYS);

    /* Arrows */
    KEY_MAPPING(126, KEY_UP);
    KEY_MAPPING(125, KEY_DOWN);
    KEY_MAPPING(123, KEY_LEFT);
    KEY_MAPPING(124, KEY_RIGHT);

    /* Function keys */
    KEY_MAPPING(122, KEY_F1);
    KEY_MAPPING(120, KEY_F2);
    KEY_MAPPING(99, KEY_F3);
    KEY_MAPPING(118, KEY_F4);
    KEY_MAPPING(96, KEY_F5);
    KEY_MAPPING(97, KEY_F6);
    KEY_MAPPING(98, KEY_F7);
    KEY_MAPPING(100, KEY_F8);
    KEY_MAPPING(101, KEY_F9);
    KEY_MAPPING(109, KEY_F10);
    KEY_MAPPING(103, KEY_F11);
    KEY_MAPPING(111, KEY_F12);

    default:
        return (Key)-1;
    }
}

static CVReturn DisplayLinkCallback(CVDisplayLinkRef displayLink,
                                    const CVTimeStamp* now,
                                    const CVTimeStamp* outputTime,
                                    CVOptionFlags flagsIn,
                                    CVOptionFlags* flagsOut,
                                    void* displayLinkContext)
{
    AppDelegate* appDelegate = (__bridge AppDelegate*)displayLinkContext;
    [appDelegate update];
    [appDelegate render];
    return kCVReturnSuccess;
}

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    /* Insert code here to initialize your application */
    atexit(sys_unlock_and_show_cursor);
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    [self shutdownDisplayLink];
    /* TODO: Shutdown code */
    destroy_game(_game);
    return NSTerminateNow;
    (void)sizeof(sender);
}
- (void)dealloc
{
    [self shutdownDisplayLink];
}
-(void) initDisplayLinkWithView:(NSOpenGLView*)view
{
    int vsync = 1;
	CGLContextObj cglContext = (CGLContextObj)[[view openGLContext] CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[view pixelFormat] CGLPixelFormatObj];
    CGLSetParameter(cglContext, kCGLCPSwapInterval, &vsync);

    /* Perform self initialization */
    [[self window] makeKeyAndOrderFront:nil];
    [self setView:view];
    /* TODO: Initialization */
    _game = create_game();

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink, &DisplayLinkCallback, (__bridge void*)self);
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displayLink);

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(windowWillClose:)
												 name:NSWindowWillCloseNotification
											   object:[self window]];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(windowResized:)
												 name:NSWindowDidResizeNotification
											   object:[self window]];
}
-(void) shutdownDisplayLink
{
	CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
}
- (void) windowWillClose:(NSNotification*)notification
{
	CVDisplayLinkStop(displayLink);
}
- (void) update
{
    NSOpenGLView* view = [self view];
	[[view openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[view openGLContext] CGLContextObj]);
    /* TODO: Update */
    if(update_game(_game))
        [NSApp performSelector:@selector(terminate:) withObject:self];
    
	CGLUnlockContext((CGLContextObj)[[view openGLContext] CGLContextObj]);
}
- (void) render
{
    NSOpenGLView* view = [self view];
	[[view openGLContext] makeCurrentContext];
	CGLLockContext((CGLContextObj)[[view openGLContext] CGLContextObj]);

    /* TODO: Render */
    render_game(_game);
	CGLFlushDrawable((CGLContextObj)[[view openGLContext] CGLContextObj]);

	CGLUnlockContext((CGLContextObj)[[view openGLContext] CGLContextObj]);
}
-(void) windowResized:(NSNotification *)notification
{
	NSRect viewRectPoints = [[self view] bounds];
    NSRect viewRectPixels = [[self view] convertRectToBacking:viewRectPoints];

    SystemEvent event;
    event.type = EVENT_RESIZE;
    event.data.resize.width = (int)viewRectPixels.size.width;
    event.data.resize.height = (int)viewRectPixels.size.height;
    _sys_push_event(event);

    [self render];
}

- (void)keyDown:(NSEvent *)theEvent
{
    Key key = _convert_keycode([theEvent keyCode]);
    if(key == KEY_INVALID)
        return;
    if(sys_key_status(key)== 0) {
        SystemEvent event;
        event.type = EVENT_KEYDOWN;
        event.data.key = key;
        _sys_push_event(event);
    }
    _sys_set_key_status(key, 1);
}
- (void)keyUp:(NSEvent *)theEvent
{
    Key key = _convert_keycode([theEvent keyCode]);
    if(key == KEY_INVALID)
        return;
    _sys_set_key_status(key, 0);
}
- (void)flagsChanged:(NSEvent *)theEvent
{
    if([theEvent modifierFlags] & NSCommandKeyMask)
        _sys_set_key_status(KEY_SYS, 1);
    else
        _sys_set_key_status(KEY_SYS, 0);

    if([theEvent modifierFlags] & NSShiftKeyMask)
        _sys_set_key_status(KEY_SHIFT, 1);
    else
        _sys_set_key_status(KEY_SHIFT, 0);

    if([theEvent modifierFlags] & NSControlKeyMask)
        _sys_set_key_status(KEY_CTRL, 1);
    else
        _sys_set_key_status(KEY_CTRL, 0);

    if([theEvent modifierFlags] & NSAlternateKeyMask)
        _sys_set_key_status(KEY_ALT, 1);
    else
        _sys_set_key_status(KEY_ALT, 0);
}
- (void)mouseDown:(NSEvent *)theEvent
{
    if(sys_mouse_button_status(MOUSE_LEFT) == 0) {
        CGPoint pt = [theEvent locationInWindow];
        SystemEvent event;
        pt = [[[self window] contentView] convertPointToBacking:pt];
        event.type = EVENT_MOUSE_DOWN;
        event.data.mouse.x = (float)pt.x;
        event.data.mouse.y = (float)pt.y;
        event.data.mouse.button = MOUSE_LEFT;
        _sys_push_event(event);
    }
    _sys_set_mouse_button_status(MOUSE_LEFT, 1);
}
- (void)mouseUp:(NSEvent *)theEvent
{
    CGPoint pt = [theEvent locationInWindow];
    SystemEvent event;
    pt = [[[self window] contentView] convertPointToBacking:pt];
    event.type = EVENT_MOUSE_UP;
    event.data.mouse.x = (float)pt.x;
    event.data.mouse.y = (float)pt.y;
    event.data.mouse.button = MOUSE_LEFT;
    _sys_push_event(event);
    _sys_set_mouse_button_status(MOUSE_LEFT, 0);
}
- (void)mouseMoved:(NSEvent *)theEvent
{
    SystemEvent event;
    event.type = EVENT_MOUSE_MOVE;
    event.data.mouse.x = (float)[theEvent deltaX];
    event.data.mouse.y = (float)[theEvent deltaY];
    _sys_push_event(event);
}
- (void)mouseDragged:(NSEvent *)theEvent { [self mouseMoved:theEvent]; }
- (void)rightMouseDown:(NSEvent *)theEvent
{
    if(sys_mouse_button_status(MOUSE_RIGHT) == 0) {
        CGPoint pt = [theEvent locationInWindow];
        SystemEvent event;
        pt = [[[self window] contentView] convertPointToBacking:pt];
        event.type = EVENT_MOUSE_DOWN;
        event.data.mouse.x = (float)pt.x;
        event.data.mouse.y = (float)pt.y;
        event.data.mouse.button = MOUSE_RIGHT;
        _sys_push_event(event);
    }
    _sys_set_mouse_button_status(MOUSE_RIGHT, 1);
}
- (void)rightMouseUp:(NSEvent *)theEvent
{
    CGPoint pt = [theEvent locationInWindow];
    SystemEvent event;
    pt = [[[self window] contentView] convertPointToBacking:pt];
    event.type = EVENT_MOUSE_UP;
    event.data.mouse.x = (float)pt.x;
    event.data.mouse.y = (float)pt.y;
    event.data.mouse.button = MOUSE_RIGHT;
    _sys_push_event(event);

    _sys_set_mouse_button_status(MOUSE_RIGHT, 0);
}
- (void)rightMouseDragged:(NSEvent *)theEvent { [self mouseMoved:theEvent]; }

@end
