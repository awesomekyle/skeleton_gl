/** @file WindowController.m
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#import "WindowController.h"

@interface WindowController ()

@end

@implementation WindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        /* Initialization code here. */
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];

    /* Implement this method to handle any initialization after your window controller's window has been loaded from its nib file. */
}

-(BOOL) acceptsFirstResponder
{
    return YES;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
- (void) keyDown:(NSEvent *)event { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)keyUp:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)flagsChanged:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)mouseDown:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)mouseUp:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)mouseMoved:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)mouseDragged:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)rightMouseDown:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)rightMouseUp:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
- (void)rightMouseDragged:(NSEvent *)event  { [[NSApp delegate] performSelector:_cmd withObject:event]; }
#pragma clang diagnostic pop

@end
