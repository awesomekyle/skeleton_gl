#include <jni.h>
#include <sys/types.h>
#include <android/asset_manager_jni.h>
#include "gl_include.h"
#include "game.h"

#define UNUSED_PARAMETER(param) (void)sizeof((param))

static Game* _game = NULL;
extern AAssetManager* _asset_manager;

JNIEXPORT void JNICALL Java_com_kyleweicht_xkcd_XKCDLib_init(JNIEnv * env, jobject obj, int width, int height)
{
    /* TODO: Initialization code */
    ASSERT_GL(glViewport(0,0,width,height));
    ASSERT_GL(glClearColor(1,0,1,1));
    _game = create_game();

    UNUSED_PARAMETER(env);
    UNUSED_PARAMETER(obj);
}
JNIEXPORT void JNICALL Java_com_kyleweicht_xkcd_XKCDLib_init_1asset_1manager(JNIEnv * env, jobject obj, jobject assetManager)
{
    _asset_manager = AAssetManager_fromJava(env, assetManager);

    UNUSED_PARAMETER(env);
    UNUSED_PARAMETER(obj);
}
JNIEXPORT void JNICALL Java_com_kyleweicht_xkcd_XKCDLib_frame(JNIEnv * env, jobject obj)
{
    /* TODO: Per-frame */
    ASSERT_GL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
    update_game(_game);
    render_game(_game);

    UNUSED_PARAMETER(env);
    UNUSED_PARAMETER(obj);
}
JNIEXPORT void JNICALL Java_com_kyleweicht_xkcd_XKCDLib_touch_1down(JNIEnv * env, jobject obj, int index, float x, float y)
{
    SystemEvent point = {0};
    point.data.touch.x = x;
    point.data.touch.y = y;
    point.data.touch.index = index;
    point.type = EVENT_TOUCH_DOWN;
    _sys_push_event(point);

    UNUSED_PARAMETER(point);
    UNUSED_PARAMETER(env);
    UNUSED_PARAMETER(obj);
}
JNIEXPORT void JNICALL Java_com_kyleweicht_xkcd_XKCDLib_touch_1up(JNIEnv * env, jobject obj, int index, float x, float y)
{
    SystemEvent point = {0};
    point.data.touch.x = x;
    point.data.touch.y = y;
    point.data.touch.index = index;
    point.type = EVENT_TOUCH_UP;
    _sys_push_event(point);

    UNUSED_PARAMETER(point);
    UNUSED_PARAMETER(env);
    UNUSED_PARAMETER(obj);
}

JNIEXPORT void JNICALL Java_com_kyleweicht_xkcd_XKCDLib_touch_1move(JNIEnv * env, jobject obj, int index, float x, float y)
{
    SystemEvent point = {0};
    point.data.touch.x = x;
    point.data.touch.y = y;
    point.data.touch.index = index;
    point.type = EVENT_TOUCH_MOVE;
    _sys_push_event(point);

    UNUSED_PARAMETER(point);
    UNUSED_PARAMETER(env);
    UNUSED_PARAMETER(obj);
}
