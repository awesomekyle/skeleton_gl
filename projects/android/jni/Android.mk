LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

INCLUDES    +=  -I../../src -I../../external -I../../
DEFINES     +=

C_STD   = -std=gnu89
CXX_STD = -std=c++98
WARNINGS    +=

LOCAL_MODULE    := xkcd
LOCAL_CFLAGS    := $(INCLUDES) $(WARNINGS) $(C_STD)
LOCAL_CXXFLAGS  := $(INCLUDES) $(WARNINGS) $(CXX_STD)
LOCAL_SRC_FILES := 	jni.c \
					../../../src/android/system_android.c \
					../../../src/timer.c \
					../../../src/system.c \
					../../../src/utility.c \
					../../../src/game.c

LOCAL_LDLIBS := -lGLESv3 -lEGL -llog -landroid

include $(BUILD_SHARED_LIBRARY)