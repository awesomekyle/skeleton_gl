package com.kyleweicht.xkcd;

import android.content.res.AssetManager;

public class XKCDLib
{

    // This is static so it will be called at startup (without creating an instance of this class)
     static 
     {
         System.loadLibrary( "xkcd" );
     }

    // Define native functions that can be called from Java code
     public static native void init( int width, int height );
     public static native void frame();
     public static native void init_asset_manager( AssetManager asset_manager );
}
