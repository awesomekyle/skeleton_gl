package com.kyleweicht.xkcd;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.content.res.AssetManager;

public class XKCD extends Activity
{
    AssetManager _asset_manager;
    XKCDView _view;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        _view = (XKCDView)findViewById(R.id.xkcd_view);
    }

    @Override protected void onPause() 
    {
        super.onPause();
        _view.onPause();
    }

    @Override protected void onResume() 
    {
        super.onResume();
        _view.onResume();
    }
}
