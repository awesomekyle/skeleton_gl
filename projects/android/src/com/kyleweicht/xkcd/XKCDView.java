package com.kyleweicht.xkcd;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.MotionEvent;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

class XKCDView extends GLSurfaceView
{
    private XKCDRenderer _renderer;

    public XKCDView( Context context ) 
    {
        super( context );
        init();
    }

    public XKCDView( Context context, AttributeSet attrs )
    {
        super( context, attrs );
        init();
    }

    public void init()
    {
        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion( 2 );
        setEGLConfigChooser(8,8,8,8,16,0);
        
        // Set the renderer associated with this view
        _renderer = new XKCDRenderer();
        setRenderer( _renderer );
    }

    private static class XKCDRenderer implements GLSurfaceView.Renderer 
    {
        public void onSurfaceCreated(GL10 gl, EGLConfig config)
        {
        }
        public void onSurfaceChanged(GL10 gl, int width, int height)
        {
            XKCDLib.init(width, height);
        }
        public void onDrawFrame(GL10 gl)
        {
            XKCDLib.frame();
        }
    }
}

