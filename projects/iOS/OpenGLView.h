#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#include "game.h"

@interface OpenGLView : GLKViewController

@property(strong, nonatomic) EAGLContext* context;
@property struct Game* game;

@end
