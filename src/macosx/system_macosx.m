/*! @file system_macosx.mm
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#include "../system.h"
#import <Foundation/Foundation.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/sysctl.h>
#include "assert.h"
#include "global.h"

/* Defines
 */
#define min(a,b) ((a) < (b) ? (a) : (b))

/* Types
 */

/* Constants
 */

/* Variables
 */

/* Internal functions
 */

/* External functions
 */
int sys_load_file_data(const char* filename, void** data, size_t* data_size)
{
    FILE* file = NULL;
    NSString* full_path = nil;
    NSString* adjusted_relative_path = [@"/assets/" stringByAppendingString:[NSString stringWithUTF8String:filename]];
    full_path = [[NSBundle mainBundle] pathForResource:adjusted_relative_path ofType:nil];

    file = fopen([full_path UTF8String], "rb");
    assert(file);

    fseek(file, 0, SEEK_END);
    *data_size = (size_t)ftell(file);
    fseek(file, 0, SEEK_SET);

    *data = calloc(1,*data_size + 1);
    assert(*data);

    fread(*data, *data_size, 1, file);
    assert(ferror(file) == 0);
    fclose(file);

    return 0;
}
void sys_free_file_data(void* data)
{
    free(data);
}
void sys_log(const char* format, ...)
{
    va_list args;
    char message[1024] = {0};
    va_start(args, format);
    vsnprintf(message, sizeof(message), format, args);
    fprintf(stderr, "%s", message);
    va_end(args);
}
int sys_num_hardware_threads(void)
{
    int num_threads = 0;
    int mib[] = {CTL_HW, HW_AVAILCPU};
    size_t  length = sizeof( num_threads );

    sysctl(mib, (uint32_t)ARRAY_LENGTH(mib), &num_threads, &length, NULL, (size_t)0);

    if( num_threads < 1 ) {
        mib[1] = HW_NCPU;
        sysctl(mib, (uint32_t)ARRAY_LENGTH(mib), &num_threads, &length, NULL, (size_t)0);
    }
    return num_threads;
}
void sys_lock_and_hide_cursor(void)
{
    CGAssociateMouseAndMouseCursorPosition(false);
    CGDisplayHideCursor(kCGDirectMainDisplay);
}
void sys_unlock_and_show_cursor(void)
{
    CGAssociateMouseAndMouseCursorPosition(true);
    CGDisplayShowCursor(kCGDirectMainDisplay);
}

