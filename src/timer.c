/*! @file timer.cpp
 *  @copyright Copyright (c) 2014 Kyle Weicht. All rights reserved.
 */
#include "timer.h"

#if defined(__APPLE__)

#include <mach/mach_time.h>
void reset_timer(Timer* T)
{
    mach_timebase_info_data_t frequency = {0,0};
    mach_timebase_info(&frequency);
    T->_frequency = (double)frequency.numer / (double)frequency.denom;
    T->_frequency *= 1e-9;
    T->_start_time = T->_prev_time = mach_absolute_time();
}
double get_delta_time(Timer* T)
{
    uint64_t current_time = mach_absolute_time();
    double delta_time = (double)(current_time - T->_prev_time) * T->_frequency;
    T->_prev_time = current_time;
    return delta_time;
}
double get_running_time(const Timer* T)
{
    uint64_t current_time = mach_absolute_time();
    double running_time = (double)(current_time - T->_start_time) * T->_frequency;
    return running_time;
}

#elif defined(_WIN32)

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

void reset_timer(Timer* T) 
{
    LARGE_INTEGER freq;
    QueryPerformanceFrequency( &freq );
    T->_frequency = 1.0/(double)freq.QuadPart;
    QueryPerformanceCounter((LARGE_INTEGER*)&T->_start_time);
    T->_prev_time = T->_start_time;
}
double get_delta_time(Timer* T)
{
    uint64_t current_time;
    double delta_time;
    QueryPerformanceCounter((LARGE_INTEGER*)&current_time);
    delta_time = (current_time - T->_prev_time) * T->_frequency;
    T->_prev_time = current_time;
    return delta_time;
}
double get_running_time(const Timer* T)
{
    uint64_t current_time;
    double  running_time;
    QueryPerformanceCounter( (LARGE_INTEGER*)&current_time );
    running_time = (current_time - T->_start_time) * T->_frequency;
    return running_time;
}

#elif defined(__linux__)

#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include <time.h>

static struct timespec _time_difference(struct timespec a, struct timespec b)
{
    struct timespec temp;
    temp.tv_sec = a.tv_sec-b.tv_sec;
    temp.tv_nsec = a.tv_nsec-b.tv_nsec;
    return temp;
}

void reset_timer(Timer* T)
{
    struct timespec time;
    clock_gettime(CLOCK_MONOTONIC, &time);
    T->_prev_time = T->_start_time = time;
}
double get_delta_time(Timer* T)
{
    struct timespec time;
    struct timespec diff;
    clock_gettime(CLOCK_MONOTONIC, &time);
    diff = _time_difference(time, T->_prev_time);
    T->_prev_time = time;
    return diff.tv_sec + diff.tv_nsec*1.0/1000000000;
}
double get_running_time(const Timer* T)
{
    struct timespec time;
    struct timespec diff;
    clock_gettime(CLOCK_MONOTONIC, &time);
    diff = _time_difference(time, T->_start_time);
    return diff.tv_sec + diff.tv_nsec*1.0/1000000000;
}

#else
    #error Need a timer
#endif
