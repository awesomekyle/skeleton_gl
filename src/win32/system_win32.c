/*! @file system_win32.mm
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#include "../system.h"
#include <Windows.h>
#include <Shlwapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "assert.h"
#include "global.h"

/* Defines
 */

/* Types
 */

/* Constants
 */

/* Variables
 */

/* Internal functions
 */

/* External functions
 */
int sys_load_file_data(const char* filename, void** data, size_t* data_size)
{
    char path[1024] = {0};
    //char dir[256] = {0};
    FILE*   file = NULL;

    // Build path
    //GetModuleFileName(NULL, dir, sizeof(dir));
    //PathRemoveFileSpec(dir);
    sprintf(path, "assets\\%s", filename);

    // Load file
    file = fopen(path, "rb");
    assert(file);

    fseek(file, 0, SEEK_END);
    *data_size = (size_t)ftell(file);
    fseek(file, 0, SEEK_SET);

    *data = calloc(1,*data_size + 1);
    assert(*data);

    fread(*data, *data_size, 1, file);
    assert(ferror(file) == 0);
    fclose(file);

    return 0;
    (void)sizeof(data_size);
}
void sys_free_file_data(void* data)
{
    free(data);
}
void sys_log(const char* format, ...)
{
    va_list args;
    char message[1024] = {0};
    va_start(args, format);
    vsnprintf(message, sizeof(message), format, args);
    fprintf(stderr, "%s", message);
    OutputDebugString(message);
    va_end(args);
}
int sys_num_hardware_threads(void)
{
    return 1;
}

