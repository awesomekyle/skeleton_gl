/** @file system.h
 *  @brief OS abstraction
 *  @copyright Copyright (c) 2014 Kyle Weicht. All rights reserved.
 */
#ifndef __system_h__
#define __system_h__

#include <stddef.h>
#include <stdint.h>
#include "vec_math.h"

typedef enum Key
{
    KEY_0,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,

    KEY_A,
    KEY_B,
    KEY_C,
    KEY_D,
    KEY_E,
    KEY_F,
    KEY_G,
    KEY_H,
    KEY_I,
    KEY_J,
    KEY_K,
    KEY_L,
    KEY_M,
    KEY_N,
    KEY_O,
    KEY_P,
    KEY_Q,
    KEY_R,
    KEY_S,
    KEY_T,
    KEY_U,
    KEY_V,
    KEY_W,
    KEY_X,
    KEY_Y,
    KEY_Z,

    KEY_SPACE,

    KEY_ESCAPE,
    KEY_SHIFT,
    KEY_CTRL,
    KEY_ALT,

    KEY_ENTER,

    KEY_SYS, /* Windows/Command key */

    KEY_UP,
    KEY_DOWN,
    KEY_RIGHT,
    KEY_LEFT,

    KEY_F1,
    KEY_F2,
    KEY_F3,
    KEY_F4,
    KEY_F5,
    KEY_F6,
    KEY_F7,
    KEY_F8,
    KEY_F9,
    KEY_F10,
    KEY_F11,
    KEY_F12,

    KEY_MAX_KEYS,
    KEY_INVALID = -1
} Key;

typedef enum MouseButton
{
    MOUSE_LEFT,
    MOUSE_RIGHT,
    MOUSE_MIDDLE,

    MOUSE_MAX_BUTTONS
} MouseButton;

typedef enum SystemEventType
{
    EVENT_RESIZE,
    EVENT_KEYDOWN,
    EVENT_KEYUP,
    EVENT_MOUSE_MOVE,
    EVENT_MOUSE_DOWN,
    EVENT_MOUSE_UP,
    EVENT_TOUCH_DOWN,
    EVENT_TOUCH_UP,
    EVENT_TOUCH_MOVE
} SystemEventType;

typedef struct SystemEvent
{    union {
        struct {
            float x; /* For up/down, this is the position */
            float y; /* For moves, this is the delta     */
            int   index;
        } touch;
        struct {
            float x; /* For clicks, this is the position */
            float y; /* For moves, this is the delta     */
            MouseButton button;
        } mouse;
        struct {
            int width;
            int height;
        } resize;
        Key key;
    } data;
    SystemEventType type;
} SystemEvent;

#ifdef __cplusplus
extern "C" {
#endif

/** @return 0 on success, -1 on failure
 */
int sys_load_file_data(const char* filename, void** data, size_t* data_size);
void sys_free_file_data(void* data);
/** Prints a message to the systems log
 */
void sys_log(const char* format, ...);

/*! @brief Returns the number of hardware threads */
int sys_num_hardware_threads(void);

/*! @brief Retrives the top system event */
SystemEvent* sys_pop_event(void);
void _sys_push_event(SystemEvent event);

/*! @brief Hides the cursor and locks it to the window */
void sys_lock_and_hide_cursor(void);
/*! @brief Shows the cursor and unlocks it */
void sys_unlock_and_show_cursor(void);

int sys_key_status(Key key);
void _sys_set_key_status(Key key, char status);

int sys_mouse_button_status(MouseButton button);
void _sys_set_mouse_button_status(MouseButton button, char status);


#ifdef __cplusplus
} // extern "C" {
#endif

#endif /* include guard */
