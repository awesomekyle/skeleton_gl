/*! @file gl_include.h
 *  @brief Abstract out platform OpenGL include files
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#ifndef __gl_include_h__
#define __gl_include_h__

#if defined(__APPLE__)
    #include <TargetConditionals.h>
    #if TARGET_OS_IPHONE == 0 && TARGET_IPHONE_SIMULATOR == 0
        #define OPENGLES 0
        #include <OpenGL/gl3.h>
        #include <OpenGL/gl3ext.h>
    #else
        #define OPENGLES 1
        #include <OpenGLES/ES3/gl.h>
        #include <OpenGLES/ES3/glext.h>
    #endif
#elif defined(__ANDROID__)
    #define OPENGLES 1
    #include <GLES3/gl3.h>
#elif defined(_WIN32)
    #define OPENGLES 0
    #undef ARRAYSIZE
    #include <gl/glew.h>
    #include <gl/wglew.h>
    #include <gl/glcorearb.h>
#elif defined(__linux__)
    #define OPENGLES 0
    #include <GL/glew.h>
    #include <GL/glxew.h>
#else
    #error Need an OpenGL implementation
#endif
#include "assert.h"
#include "system.h"

#if OPENGLES
    #define glClearDepth glClearDepthf
#endif

/** @brief OpenGL Error code strings
 */
#define STATUS_CASE(enum) case enum: return #enum
static const char* _glStatusString(GLenum error)
{
    switch(error) {
        STATUS_CASE(GL_NO_ERROR);
        STATUS_CASE(GL_INVALID_ENUM);
        STATUS_CASE(GL_INVALID_VALUE);
        STATUS_CASE(GL_INVALID_OPERATION);
        STATUS_CASE(GL_INVALID_FRAMEBUFFER_OPERATION);
        STATUS_CASE(GL_OUT_OF_MEMORY);
        STATUS_CASE(GL_FRAMEBUFFER_COMPLETE);
        STATUS_CASE(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT);
        #if !defined(__gl_h_) && !defined(__gl3_h_)
            STATUS_CASE(GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS); /* Only exists in GLES */
        #endif
        STATUS_CASE(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT);
        STATUS_CASE(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE);
        STATUS_CASE(GL_FRAMEBUFFER_UNSUPPORTED);
        #if defined(__gl_h)
            STATUS_CASE(GL_STACK_OVERFLOW);
            STATUS_CASE(GL_STACK_UNDERFLOW);
            STATUS_CASE(GL_TABLE_TOO_LARGE);
        #endif
    }
    return "Unknown error";
}
#undef STATUS_CASE

/** @brief OpenGL Error checking wrapper
 */
#ifndef ASSERT_GL
    #if defined(__APPLE__) || defined(__GNUG__)
        #define ERROR_FORMAT "%s:%d: "
    #else
        #define ERROR_FORMAT "%s(%d): "
    #endif
    #ifndef NDEBUG
        #define ASSERT_GL(x)                                    \
            do {                                                \
                GLenum _glError;                                \
                x;                                              \
                _glError = glGetError();                        \
                if(_glError != GL_NO_ERROR) {                   \
                    sys_log(ERROR_FORMAT"  %s Error: %s\n",        \
                                __FILE__, __LINE__,             \
                                #x, _glStatusString(_glError)); \
                }                                               \
            } while(__LINE__ == -1)
    #else
        #define ASSERT_GL(x) x
    #endif /* NDEBUG */
#endif /* #ifndef ASSERT_GL */

/** @brief Manual OpenGL error checking
 */
#ifndef CheckGLError
    #define CheckGLError()                              \
        do {                                            \
            GLenum _glError = glGetError();             \
            if(_glError != GL_NO_ERROR) {               \
                sys_log("%s:%d:  OpenGL Error: %s\n",\
                            __FILE__, __LINE__,         \
                            _glStatusString(_glError));  \
            }                                           \
        } while(__LINE__ == -1)
#endif /* #ifndef CheckGLError */

/** @brief Constants for defining shader input slots (like DX)
 */
enum
{
    POSITION    = 0,
    NORMAL      = 1,
    TANGENT     = 2,
    BITANGENT   = 3,
    TEXCOORD0   = 5,
    TEXCOORD1   = 6,
    COLOR       = 7
};

#endif
