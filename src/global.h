/*! @file global.h
 *  @brief Global defines
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#ifndef __global_h__
#define __global_h__

#include "platform.h"
#include "assert.h"

#ifndef ARRAY_LENGTH
    #define ARRAY_LENGTH(a) (sizeof((a))/sizeof((a)[0]))
#endif

#ifndef ALIGN
    #ifdef __GNUC__
        #define ALIGN(x) __attribute__((aligned(x)))
    #elif defined(_MSC_VER)
        #define ALIGN(x) __declspec(align(x))
    #endif
#endif

#ifndef UNREFERENCED_PARAMETER
    #define UNREFERENCED_PARAMETER(p) (void)sizeof(p)
#endif

#endif /* include guard */
