/*! @file utility.h
 *  @brief Various utility functions
 *  @copyright Copyright (c) 2014 Kyle Weicht. All rights reserved.
 */
#ifndef __utility_h__
#define __utility_h__

#include <stddef.h>
#include <string.h>

/** @brief Like `fread`, but for a memory buffer
 *  @details Reads count*size bytes from src and stores it in dest. Returns
 *    The new position of src.
 *  @param[in,out] dest The buffer to read the data into
 *  @param[in] size The size of one item to copy
 *  @param[in] count The number of items to copy
 *  @param[in] src The buffer to read from
 *  @return The position of the buffer after the read.
 */
void* mread(void* dest, size_t size, size_t count, const void* src);

/** @brief Retrieves a line from a string
 *  @param line [in] A buffer to hold the retrieved line
 *  @param line_size [in] The size of the `line` buffer
 *  @param buffer [in] The buffer to parse
 *  @return A pointer to the next line in the buffer, 0 when at the end
 */
const char* get_line_from_buffer(char* line, size_t line_size, const char* buffer);

const char* get_extension_from_filename(const char* filename);

void split_filename(char* path, size_t path_size,
                    char* file, size_t file_size,
                    const char* filename);

#endif /* include guard */
