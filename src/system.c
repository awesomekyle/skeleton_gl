/*! @file system.c
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#include "system.h"
#include <stdint.h>

/* Defines
 */
enum { MAX_EVENTS = 1024 };
#define min(a,b) ((a) < (b) ? (a) : (b))

/* Types
 */

/* Constants
 */

/* Variables
 */
static char         _keys[KEY_MAX_KEYS] = {0};
static char         _mouse_buttons[MOUSE_MAX_BUTTONS] = {0};

static SystemEvent  _event_queue[MAX_EVENTS];
static uint32_t     _write_pos = 0;
static uint32_t     _read_pos = 0;

/* Internal functions
 */


/* External functions
 */
SystemEvent* sys_pop_event(void)
{
    if(_read_pos == _write_pos)
        return NULL;
    return &_event_queue[(_read_pos++) % MAX_EVENTS];
}
void _sys_push_event(SystemEvent event)
{
    _event_queue[_write_pos%MAX_EVENTS] = event;
    _write_pos++;
}
int sys_key_status(Key key)
{
    return _keys[key];
}
void _sys_set_key_status(Key key, char status)
{
    _keys[key] = status;
}
int sys_mouse_button_status(MouseButton button)
{
    return _mouse_buttons[button];
}
void _sys_set_mouse_button_status(MouseButton button, char status)
{
    _mouse_buttons[button] = status;
}
