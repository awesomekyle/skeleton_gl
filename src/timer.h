/*! @file timer.h
 *  @brief High-performance, high-resolution timer
 *  @copyright Copyright (c) 2014 Kyle Weicht. All rights reserved.
 */
#ifndef __timer_h__
#define __timer_h__

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include "platform.h"

#ifdef __GNUC__
    #include <sys/time.h>
#endif

/* Types
 */
typedef struct Timer
{
    #if PLATFORM_ID == PLATFORM_IOS || PLATFORM_ID == PLATFORM_MACOS || PLATFORM_ID == PLATFORM_WINDOWS
    uint64_t    _start_time;
    uint64_t    _prev_time;
    double      _frequency;
    #elif PLATFORM_ID == PLATFORM_LINUX || PLATFORM_ID == PLATFORM_ANDROID
    struct timespec    _start_time;
    struct timespec    _prev_time;
    #endif
} Timer;

/* Functions
 */
void reset_timer(Timer* T);
/** @return The time since the last `get_delta_time` call, in seconds. */
double get_delta_time(Timer* T);
/** @return The time since the last `reset_timer` call, in seconds. */
double get_running_time(const Timer* T);

#endif /* include guard */
