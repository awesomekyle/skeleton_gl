/*! @file game.h
 *  @copyright Copyright (c) 2014 Kyle Weicht. All rights reserved.
 */
#ifndef __game_h__
#define __game_h__

/* Types
 */
typedef struct Game Game;

/* Functions
 */
Game* create_game(void);
int update_game(Game* G);
void render_game(Game* G);
void destroy_game(Game* G);

#endif /* include guard */
