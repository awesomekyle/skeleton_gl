/** @file game.c
 *  @copyright Copyright (c) 2014 Kyle Weicht. All rights reserved.
 */
#include "game.h"
#include <stdlib.h>
#include "global.h"
#include "timer.h"
#include "system.h"

/* Defines
 */

/* Types
 */
struct Game
{
    Timer   timer;

    int frame;
};

/* Constants
 */

/* Variables
 */

/* Internal functions
 */
static int _update_game(Game* G, float dt)
{
    /* Process system events
     */
    SystemEvent* event = sys_pop_event();
    while(event) {
        switch(event->type) {
            case EVENT_KEYDOWN:
                switch(event->data.key) {
                    case KEY_ESCAPE:
                        return 1;
                    default:
                        break;
                }
                break;
            #if FORM_FACTOR == FORM_DESKTOP
            case EVENT_MOUSE_DOWN:
                if(event->data.mouse.button == MOUSE_LEFT)
                    sys_lock_and_hide_cursor();
                break;
            case EVENT_MOUSE_UP:
                if(event->data.mouse.button == MOUSE_LEFT)
                    sys_unlock_and_show_cursor();
                break;
            #endif /* #if FORM_FACTOR == FORM_DESKTOP */
            default:
                break;
        }
        event = sys_pop_event();
    }

    UNREFERENCED_PARAMETER(G);
    UNREFERENCED_PARAMETER(dt);
    return 0;
}

/* External functions
 */
Game* create_game(void)
{
    Game* G = NULL;

    /* Allocate game */
    G = (Game*)calloc(1, sizeof(Game));
    
    reset_timer(&G->timer);
    return G;
}
int update_game(Game* G)
{
    static const int target_fps = 128;
    static int update_fps_frame = 0;
    static float accumulator = 0.0f;
    static float fps_time = 0.0f;
    static int fps_frame = 0;
    
    int result = 0;
    float frame_time = (float)get_delta_time(&G->timer);
    float delta_time = 1.0f/target_fps;
    
    if(frame_time > 0.25f)
        frame_time = 0.25f;
    
    accumulator += frame_time;

    while(accumulator >= delta_time) {
        result = _update_game(G, delta_time);
        if(result)
            return 1;
        accumulator -= delta_time;
        update_fps_frame++;
    }
    
    /* FPS
     */
    if(fps_time >= 1.0f) {
        float ms = fps_time/fps_frame;
        sys_log("%f ms (%d FPS, %d UPS)\n", ms*1000, fps_frame, update_fps_frame);
        fps_time -= 1.0f;
        fps_frame = 0;
        update_fps_frame = 0;
    }
    fps_time += frame_time;
    fps_frame++;
    G->frame++;
    
    return 0;
}
void render_game(Game* G)
{
    UNREFERENCED_PARAMETER(G);
}
void destroy_game(Game* G)
{
    free(G);
}
