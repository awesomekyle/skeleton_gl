/** @file platform.h
 *  @brief Platform abstraction defines
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#ifndef __platform_h__
#define __platform_h__

/** Configuration detection
 */
#if defined( DEBUG ) || defined( _DEBUG )
    #ifndef DEBUG
        #define DEBUG
    #endif
#else
    #ifndef RELEASE
        #define RELEASE
    #endif
#endif


/** Platform detection
 */
#define PLATFORM_WINDOWS 1
#define PLATFORM_MACOS   2
#define PLATFORM_LINUX   3
#define PLATFORM_IOS     4
#define PLATFORM_ANDROID 5

#if defined( WIN32 )
    #define PLATFORM_ID PLATFORM_WINDOWS
    #ifndef WIN32_LEAN_AND_MEAN
        #define WIN32_LEAN_AND_MEAN
    #endif
#elif defined( __MACH__ )
    #include <TargetConditionals.h>
    #if( TARGET_OS_IPHONE )
        #define PLATFORM_ID PLATFORM_IOS
    #else
        #define PLATFORM_ID PLATFORM_MACOS
    #endif
#elif defined( __ANDROID__ )
    #define PLATFORM_ID PLATFORM_ANDROID
#elif defined( __linux__ )
    #define PLATFORM_ID PLATFORM_LINUX
#else
    #error No valid target found
#endif

/** ISA detection
 */
#define ISA_IA32 1
#define ISA_ARM  2

#if defined(__arm__)
    #define ISA_ID ISA_ARM
#else
    #define ISA_ID ISA_IA32
#endif

/** Architecture detection
 */
#define ARCH_64BIT 1
#define ARCH_32BIT 2

#if defined( _M_X64 ) || \
    defined( __LP64__ ) || \
    defined( __aarch64__ ) || \
    (defined(TARGET_RT_64_BIT) && ( TARGET_RT_64_BIT == 1 ))
    #define ARCH_ID ARCH_64BIT
#else
    #define ARCH_ID ARCH_32BIT
#endif

/**System information
 */
#define SYSTEM_POSIX    1
#define SYSTEM_WINDOWS  2

#if PLATFORM_ID == PLATFORM_MACOS || \
    PLATFORM_ID == PLATFORM_LINUX || \
    PLATFORM_ID == PLATFORM_IOS   || \
    PLATFORM_ID == PLATFORM_ANDROID
    #define SYSTEM_TYPE SYSTEM_POSIX
#elif PLATFORM_ID == PLATFORM_WINDOWS
    #define SYSTEM_TYPE SYSTEM_WINDOWS
#else
    #error Unsupported system
#endif

/** Form factor
 */
#define FORM_DESKTOP    1
#define FORM_MOBILE     2

#if PLATFORM_ID == PLATFORM_MACOS || \
    PLATFORM_ID == PLATFORM_LINUX || \
    PLATFORM_ID == PLATFORM_WINDOWS
    #define FORM_FACTOR FORM_DESKTOP
#elif PLATFORM_ID == PLATFORM_IOS   || \
      PLATFORM_ID == PLATFORM_ANDROID
    #define FORM_FACTOR FORM_MOBILE
#else
    #error Unsupported system
#endif

/** Debug information
 */
#if PLATFORM_ID == PLATFORM_WINDOWS
    #define PLATFORM_NAME "Microsoft Windows"
#elif PLATFORM_ID == PLATFORM_IOS
    #define PLATFORM_NAME "Apple iOS"
#elif PLATFORM_ID == PLATFORM_MACOS
    #define PLATFORM_NAME "Apple Mac OS X"
#elif PLATFORM_ID == PLATFORM_ANDROID
    #define PLATFORM_NAME "Google Android"
#elif PLATFORM_ID == PLATFORM_LINUX
    #define PLATFORM_NAME "Linux"
#endif

#if ISA_ID == ISA_IA32
    #define ISA_NAME "Intel x86"
#elif ISA_ID == ISA_ARM
    #define ISA_NAME "ARM"
#endif

#if ARCH_ID == ARCH_64BIT
    #define ARCH_NAME "64-bit"
#elif ARCH_ID == ARCH_32BIT
    #define ARCH_NAME "32-bit"
#endif

static __inline const char* system_information(void)
{
    return PLATFORM_NAME", "ISA_NAME", "ARCH_NAME;
}

#endif /* include guard */
